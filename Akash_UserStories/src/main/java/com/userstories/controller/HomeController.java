package com.userstories.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.userstories.pojo.UserLogin;
import com.userstories.service.UserLoginService;

@Controller
public class HomeController {

	@Autowired
	private UserLoginService loginservice;

	public HomeController() {
		System.out.println("Home Controller");
	}

	@GetMapping("/")
	public String getIndex() {
		return "index";
	}

	@GetMapping("/back")
	public String getBack() {
		return "dashboardview";
	}

	@GetMapping("/login")
	public String getLogin(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "login";
	}

	@PostMapping("/login")
	public String postLogin(@RequestParam(required = false) String register, UserLogin loginuser, HttpSession session,Map<String, String> map) {
		if (register == null) {
			try {
				if (loginservice.userValidation(loginuser)) {
					session.setAttribute("mail", loginservice.getLoginUser(loginuser.getEmail()).getEmail());
					return "dashboardview";
				} else {
					map.put("error", "Invalid creditionals!");
					return "redirect:login";
				}
			} catch (Exception e) {
				map.put("error", "User not found please register!");
				return "redirect:login";
			}

		} else {
			return "redirect:register";
		}
	}

	@GetMapping("/register")
	public String getRegister(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "register";
	}

	@PostMapping("/register")
	public String userRegister(UserLogin user,Map<String, String> map) {
		System.out.println(user);
		if(loginservice.insertUser(user)) {
			return "redirect:login?error=Registration successfull login please";
		}else {
			return "redirect:register?error=User not able to register";
		}
	}

	@GetMapping("/logout")
	public String getLogout(HttpSession httpSession) {
		httpSession.removeAttribute("email");
		return "login";
	}
}
