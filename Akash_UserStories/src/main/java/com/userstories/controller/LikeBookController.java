package com.userstories.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.userstories.pojo.Book;
import com.userstories.pojo.LikeBook;
import com.userstories.service.BookService;
import com.userstories.service.LikeBookService;

@Controller
public class LikeBookController {

	@Autowired
	private LikeBookService likedBookService;

	@Autowired
	private BookService bookService;

	public LikeBookController() {
		System.out.println("Like Book Controller");
	}

	@GetMapping("/likedbooks")
	public String viewBooks(Map<String, List<LikeBook>> map) {
		try {
			List<LikeBook> books = likedBookService.getAllBook();
			map.put("books", books);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Book list not found");
			e.printStackTrace();
		}
		return "likedbooks";
	}

	@GetMapping("/addliked")
	public String insertLikedBook(@RequestParam(value="value", required=false) int value) throws Exception {
		System.out.println(value);
		for(Book lb : bookService.getAllBook()) {
			if (lb.getId()==value) {
				try {
					System.out.println(lb);
					LikeBook lbks = new LikeBook(lb.getId(),lb.getTitle(),lb.getAuthor(),lb.getYear());
					System.out.println(lbks);
					likedBookService.insetLikedBook(lbks);
					return "redirect:/viewbooks";
				}catch(Exception e) {
					System.out.println("Fail");
				}
			}
		}
		return "redirect:/viewbooks";
	}
	
	@GetMapping("/deleteliked")
	public String deleteLikedBooks(@RequestParam(value="value", required=false) int value) throws Exception {
		System.out.println(value);
		for(LikeBook lb :likedBookService.getAllBook()) {
			if(lb.getId()==value) {
				likedBookService.deleteById(lb.getId());
				return "redirect:/likedbooks";
			}
		}
		return "redirect:/likedbooks";
	}
}