package com.userstories.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.userstories.pojo.Book;
import com.userstories.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

	public BookController() {
		System.out.println("Book Controller");
	}

	@GetMapping("/viewbooks")
	public String viewBooks(Map<String, List<Book>> map) {
		try {
			List<Book> books = bookService.getAllBook();
			map.put("books", books);
		} catch (Exception e) {
			System.out.println("Book list not found");
			e.printStackTrace();
		}
		return "booksdashboard";
	}
	
	@GetMapping("/guestview")
	public String guestView(Map<String, List<Book>> map) {
		try {
			List<Book> books = bookService.getAllBook();
			map.put("books", books);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Book list not found");
			e.printStackTrace();
		}
		return "guestview";
	}
}
