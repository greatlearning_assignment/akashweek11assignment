package com.userstories.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import com.userstories.pojo.Book;
import com.userstories.pojo.UserLogin;
import com.userstories.service.BookService;

@Controller
public class AdminController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private BookService bookService;
	
	@GetMapping("/adminregister")
	public String getRegister() {
		return "adminregister";
	}
	
	@PostMapping("/admindata")
	public String adminRegister(@ModelAttribute("email") String email, String password, HttpSession session,Map<String, String> map) {
		System.out.println(email);
		System.out.println(password);
		restTemplate.getForEntity("http://localhost:8001/"+email+"/"+password+"", String.class);
		return "login";
	}
	
	@GetMapping("/adminlogin")
	public String adminLogin() {
		return "adminlogin";
	}
	
	@PostMapping("/adminlogindata")
	public String adminLoginData(@ModelAttribute("email") String email, String password,HttpSession session) {
		session.setAttribute("mail", email);
		return "admindashboard";
	}
	
	@GetMapping("/{email}/{password}")
	public String getAdminDetails(@PathVariable String email, @PathVariable String password,HttpSession session) {
		System.out.println("Admin called");
		System.out.println(email);
		System.out.println(password);
		UserLogin adminuser = new UserLogin();
		adminuser.setEmail(email);
		adminuser.setPassword(password);
		session.setAttribute("mail", email);
		return "admindashboard";
	}
	
	@GetMapping("/adminlogout")
	public String getLogout(HttpSession httpSession) {
		httpSession.removeAttribute("email");
		return "adminlogin";
	}
	
	@GetMapping("/adminview")
	public String viewBooks(Map<String, List<Book>> map) {
		try {
			List<Book> books = bookService.getAllBook();
			map.put("books", books);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Book List not found");
			e.printStackTrace();
		}
		return "adminbooks";
	}
	
	@GetMapping("/adminback")
	public String getBack() {
		return "admindashboard";
	}
	
	@GetMapping("/usersforadmin")
	public String getUserForAdmin() {
		restTemplate.getForEntity("http://localhost:8001/getusers/", String.class);
		return "usersforadmin";
	}
}
