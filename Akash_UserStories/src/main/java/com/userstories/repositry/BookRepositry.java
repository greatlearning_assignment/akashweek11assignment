package com.userstories.repositry;

import org.springframework.data.repository.CrudRepository;

import com.userstories.pojo.Book;

public interface BookRepositry extends CrudRepository<Book, Integer>{

}
