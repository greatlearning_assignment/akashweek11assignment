package com.userstories.repositry;

import org.springframework.data.repository.CrudRepository;

import com.userstories.pojo.ReadLater;

public interface ReadLaterRepositry extends CrudRepository<ReadLater, Integer>{

}
