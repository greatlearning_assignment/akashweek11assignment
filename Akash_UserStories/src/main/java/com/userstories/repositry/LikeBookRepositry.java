package com.userstories.repositry;

import org.springframework.data.repository.CrudRepository;

import com.userstories.pojo.Book;
import com.userstories.pojo.LikeBook;

public interface LikeBookRepositry extends CrudRepository<LikeBook, Integer>{

	void save(Book lbooks);

}
