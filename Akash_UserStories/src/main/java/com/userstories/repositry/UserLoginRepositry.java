package com.userstories.repositry;

import org.springframework.data.repository.CrudRepository;

import com.userstories.pojo.UserLogin;

public interface UserLoginRepositry extends CrudRepository<UserLogin, Integer>{
	
	public UserLogin findByEmail(String email);
    
    public boolean existsByEmail(String email);

}
