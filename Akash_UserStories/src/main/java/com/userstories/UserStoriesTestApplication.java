package com.userstories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class UserStoriesTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserStoriesTestApplication.class, args);
	}
	
	@Bean
	public RestTemplate initTemplete() {
		return new RestTemplate();
	}
}
