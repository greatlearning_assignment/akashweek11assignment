package com.userstories.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userstories.pojo.Book;
import com.userstories.repositry.BookRepositry;

@Service
public class BookService {
	
	@Autowired
    private BookRepositry bookRepositry;
	
	public List<Book> getAllBook() throws Exception {
		return (List<Book>) this.bookRepositry.findAll();
	}
	
	public Book getBookById(int bookId) {
		return this.bookRepositry.findById(bookId).get();
	}
}
