package com.userstories.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userstories.pojo.UserLogin;
import com.userstories.repositry.UserLoginRepositry;

@Service
public class UserLoginService {
	
	@Autowired
	private UserLoginRepositry loginrepo;
	
	public UserLogin getLoginUser(String email) throws Exception {
		return this.loginrepo.findByEmail(email);
	}
	
	public boolean userValidation(UserLogin user) throws Exception {
		UserLogin existUser = null;
		try {
			existUser = getLoginUser(user.getEmail());
			if (user.getPassword().equals(existUser.getPassword())) {
				return true;
			}

		} catch (Exception e) {
			throw e;
		}
		return false;
	}
	
	public UserLogin getUserById(int userId) {
		return this.loginrepo.findById(userId).get();
	}

	public boolean insertUser(UserLogin user) {
		this.loginrepo.save(user);
		return true;
	}
}
