package com.userstories.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userstories.pojo.ReadLater;
import com.userstories.repositry.ReadLaterRepositry;

@Service
public class ReadLaterBookService {
	
	@Autowired
	private ReadLaterRepositry readlaterRepoService;
	
	public List<ReadLater> getAllBook() throws Exception {
		return (List<ReadLater>) this.readlaterRepoService.findAll();
	}
	
	public boolean insetReadLaterBook(ReadLater book) {
		if (this.readlaterRepoService.existsById(book.getId())) {
			return false;
		}
		this.readlaterRepoService.save(book);
		return true;
	}
	
	public boolean deleteById(int id) {
		this.readlaterRepoService.deleteById(id);
		return true;
	}
}
