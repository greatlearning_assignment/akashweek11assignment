package com.userstories.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userstories.pojo.LikeBook;
import com.userstories.repositry.LikeBookRepositry;

@Service
public class LikeBookService {

	@Autowired
	private LikeBookRepositry likedRepoService;

	public List<LikeBook> getAllBook() throws Exception {
		return (List<LikeBook>) this.likedRepoService.findAll();
	}

	public boolean insetLikedBook(LikeBook book) {
		if (this.likedRepoService.existsById(book.getId())) {
			return false;
		}
		this.likedRepoService.save(book);
		return true;
	}
	
	public boolean deleteById(int id) {
		this.likedRepoService.deleteById(id);
		return true;
	}
}
