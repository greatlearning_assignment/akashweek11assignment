<%@page import="com.userstories.pojo.Book"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isErrorPage="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Guest View</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
	integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
	crossorigin="anonymous">
<style>
body {
	background-image:url('assets/images/Background2.jpg');
	background-repeat: no-repeat;
	background-position: centre centre fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	margin: 0px;
	color: black;
	padding-left: 30px;
	padding-top: 23px;
	overflow-x: hidden;
	padding-right: 25px;
	backdrop-filter: blur(3px);
}

.main {
	padding-top: 2px;
}

hr {
	height: 2px;
	border-width: 0;
	color: black;
	background-color: grey;
	position: relative;
	left: -0.1%;
}

.lmove {
	padding-left: 20px;
}

h2 {
	font-family: Serif;
	font-weight: bold;
}

.contentsdisp {
	color: #000066;
	font-family: Serif;
	font-weight: bold;
	display: grid;
	grid-template-columns: auto auto auto auto;
	grid-gap: 15px;
	padding-left: 45px;
}

.backg {
	padding-left: 5px;
	backdrop-filter: blur(10px);
	backdrop-color: rgba(255, 255, 255, 0.5);
	padding-top: 15px;
}

.imgedit {
	width: 200px;
	height: 250px;
	border: 2px solid black;
	border-radius: 20px
}

.details {
	padding-top: 10px
}

.centre {
	padding-left: 20px;
}

a:hover {
	text-decoration: none;
}

.lrbutton {
	padding-left: 25px;
}

.lbstyle {
	border-radius: 5px;
	background: white;
	color: red;
	font-weight: bold;
}

.rbstyle {
	border-radius: 5px;
	background: white;
	color: blue;
	font-weight: bold;
}

.msgdisp{
	padding-left: 10px;
}
</style>
</head>

<body>
	<div class="main">
		<h2>
			Welcome to Online bookess store
		</h2>
		<div class="contentsdisp">
			<%
			List<Book> bookie = (List<Book>) request.getAttribute("books");

					for (Book book : bookie) {
			%>
			<div class="backg">
				<div class="center">
					<img class="imgedit" src="assets/images/<%=book.getTitle()%>.jpg"/>
				</div>
				<p class="details">
					Title :<%=book.getTitle()%></p>
				<p>
					Year :<%=book.getYear()%></p>
				<p>
					Author :<%=book.getAuthor()%></p>
			</div>
			<%} %>
		</div>
	</div>
</body>

</html>