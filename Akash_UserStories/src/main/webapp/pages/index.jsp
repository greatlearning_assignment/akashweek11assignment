<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Bookes</title>
<!-- CSS only -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
	integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
	crossorigin="anonymous">
<style>
body {
	background-image: url('assets/images/Background1.jpg');
	background-repeat: no-repeat;
	background-position: centre centre fixed;
	background-size: cover;
	overflow: auto;
	width: fit-content;
	block-size: fit-content;
}

.head {
	font-family: 'Arial', Helvetica, sans-serif;
	clip-path: black;
	padding-left: 10px;
	color: black;
	padding-top: 10px;
	font-size: 80px;
}
.sel{
	padding-left:100px;
}

.btn {
	box-sizing: border-box;
	appearance: none;
	background-color: transprent;
	border: 3px groove #92a8d1;
	border-radius: 0.7em;
	color: #92a8d1;
	cursor: crosshair;
	display: inline-block;
	font-size: 20px;
	margin: 20px;
	padding: 15px 32px;
	font-family:Arial ;
	font-weight: 500;
}

.btn:hover, .btn:focus {
	color: blue;
}

.third {
	border-color: #92a8d1;
	color:gray;
	box-shadow: 0 0 35px 40px transparent inset, 0 0 0 0 transparent;
	-webkit-transition: all 150ms ease-in-out;
	transition: all 150ms ease-in-out;
}

.third:hover {
	box-shadow: 0 0 15px 0 #92a8d1 inset, 0 0 15px 6px #92a8d1;
}

a {
	text-decoration: none;
}
}
</style>
</head>

<body>
	<div class="background">
		<div class="headding">
			<h1 class="head" align="left">
			BOOKESS
			</h1>
			<div class="sel" align="center">
				<a href='login'><button class="btn third">User Login</button></a>
			</div>
			<div class="sel" align="center">
				<a href='register'><button class="btn third">User Register</button></a>
			</div>
			<div class="sel" align="center">
				<a href='adminlogin'><button class="btn third">Admin Login</button></a>
			</div>
			<div class="sel" align="center">
				<a href='adminregister'><button class="btn third">Admin Register</button></a>
			</div>
			<div class="sel" align="center">
				<a href='guestview'><button class="btn third">Books</button></a>
			</div>
		</div>
	</div>
</body>

</html>
