package com.adminstories.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.adminstories.pojo.Admin;
import com.adminstories.service.AdminLogin;

@RestController
public class AdminController {
	
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private AdminLogin adminLogin;

	public AdminController() {
		System.out.println("Admin Controller working! ");
	}

	@GetMapping("/{email}/{password}")
	public String getAdminDetails(@PathVariable String email, @PathVariable String password) throws Exception, Exception {
		System.out.println("admin name called");
		System.out.println(email);
		System.out.println(password);
		Admin adminuser = new Admin();
		adminuser.setEmail(email);
		adminuser.setPassword(password);
		adminLogin.insertUser(adminuser);
		if(adminLogin.adminValidation(adminuser)) {
			restTemplate.getForEntity("http://localhost:8000/"+email+"/"+password+"", String.class);
		}else {
			System.out.println("Validation Failed");
		}
		return "success";
	}
	@RequestMapping("/hello")
	public String indexPage() {
		System.out.println("index page called");
		return "Welcome";
	}
	
	@GetMapping("/getusers")
	public String getUserForAdmin() {
		System.out.println(adminLogin.getContacts());
		return "Success";
	}
}

