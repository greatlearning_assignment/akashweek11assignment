package com.adminstories.repositry;

import org.springframework.data.repository.CrudRepository;

import com.adminstories.pojo.Admin;

public interface AdminRepositry extends CrudRepository<Admin, Integer>{
	public Admin findByEmail(String email);	
}
