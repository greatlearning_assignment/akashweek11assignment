package com.adminstories.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminstories.pojo.Admin;
import com.adminstories.repositry.AdminRepositry;

@Service
public class AdminLogin {
	
	@Autowired
	private AdminRepositry loginrepositry;
	
	public List<Admin> getContacts() {
		List<Admin> customers = new ArrayList<>();
		this.loginrepositry.findAll().forEach(customer->customers.add(customer));
		return customers;
	}
	
	public Admin getLoginUser(String email) throws Exception {
		return this.loginrepositry.findByEmail(email);
	}
	
	public boolean adminValidation(Admin user) throws Exception {
		Admin existUser = null;
		try {
			existUser = getLoginUser(user.getEmail());
			if (user.getPassword().equals(existUser.getPassword())) {
				return true;
			}

		} catch (Exception e) {
			throw e;
		}
		return false;
	}
	
	public Admin getUserById(int userId) {
		return this.loginrepositry.findById(userId).get();
	}

	public boolean insertUser(Admin user) {
		this.loginrepositry.save(user);
		return true;
	}
}
