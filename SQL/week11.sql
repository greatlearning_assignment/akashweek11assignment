create database bookie;
use bookies;
create table books(id int(11) primary key,title varchar(255),author varchar(255),year int(11));


INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('1', 'Fear Not Be Strong', 'Swami Akash', '1999');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('2', 'One Day Life Will Change', 'Saranya Umakanthan', '2000');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('3', 'Wings Of Fire', 'APJ Kalam', '2002');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('4', 'Belive in YourSelf', 'Dr. Joseph', '2014');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('5', 'The Time Machine', 'H G . Wells', '2011');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('6', 'Learning How To Fly', 'APJ kalam', '2009');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('7', 'Something I Never Told You', 'Shravya Bhinder', '2010');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('8', 'Relativity', 'Einstein', '1978');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('9', 'Life is What You Make It', 'Dr. Sonal', '2010');
INSERT INTO `bookie`.`books` (`id`, `title`, `author`, `year`) VALUES ('10', 'Who Will Cry When You Die', 'Robin Sharma', '2012');

use bookie;
create table userlogin(email varchar(255),password varchar(255));
INSERT INTO `bookies`.`loginuser` (`email`, `password`) VALUES ('books@gmail.com', 'books');

INSERT INTO `bookie`.`loginuser` (`email`, `password`) VALUES ('Akash@gmail.com', 'Akash123');

select * from userlogin;

use bookie;
create table likebook(id int(11) primary key,title varchar(255),author varchar(255),year int(11));

select * from likebook;


use bookie;
create table readlater(id int(11) primary key,title varchar(255),author varchar(255),year int(11));

select * from readlater;